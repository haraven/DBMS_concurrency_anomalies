USE TempWDB;
GO
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--																					VALIDATORS
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------
-- CREATURE VALIDATOR
--------------------------------------------------------------
IF OBJECT_ID (N'dbo.ufn_validate_creature', N'FN') IS NOT NULL
    DROP FUNCTION ufn_validate_creature;
GO

CREATE FUNCTION ufn_validate_creature
(
	@CreatureName     VARCHAR(255),
	@CreatureSubname  VARCHAR(255),
	@CreatureMinLevel INT,
	@CreatureMaxLevel INT,
	@Faction          INT,
	@CreatureType     INT
)
RETURNS VARCHAR(510)
AS
BEGIN
	DECLARE @errors VARCHAR(510);
	SET @errors = '';

	IF @CreatureName IS NULL OR @CreatureName = ''
		SET @errors = 'Creature name cannot be null. ';
	IF ISNUMERIC(SUBSTRING(LTRIM(@CreatureName), 1, 1)) = 1
		SET @errors = @errors + 'Creature name cannot start with a number. ';
	IF ISNUMERIC(SUBSTRING(LTRIM(@CreatureSubname), 1, 1)) = 1
		SET @errors = @errors + 'Creature subname cannot start with a number. ';
	IF @CreatureMinLevel < 1
		SET @errors = @errors + 'Creature minlevel cannot be negative (or zero). ';
	IF @CreatureMaxLevel < 1
		SET @errors = @errors + 'Creature maxLevel cannot be negative (or zero). ';
	IF @Faction < 0
		SET @errors = @errors + 'Creature faction cannot be negative. ';
	if @CreatureType < 1
		SET @errors = @errors + 'Creature type cannot be negative (or zero). ';

	RETURN @errors;
END;
GO

--------------------------------------------------------------
-- ITEM VALIDATOR
--------------------------------------------------------------

IF OBJECT_ID (N'dbo.ufn_validate_item', N'FN') IS NOT NULL
    DROP FUNCTION ufn_validate_item;
GO

CREATE FUNCTION ufn_validate_item
(
	@ItemName          VARCHAR(255),
	@ItemDisplayID     INT,
	@ItemQuality       INT,
	@ItemStackLimit    INT,
	@ItemBuyPrice      INT,
	@ItemSellPrice     INT,
	@ItemLevel         INT,
	@ItemRequiredLevel INT
)
RETURNS VARCHAR(510)
AS
BEGIN
	DECLARE @errors VARCHAR(510);
	SET @errors = '';

	IF @ItemName IS NULL OR @ItemName = ''
		SET @errors = 'Item name cannot be null. ';
	IF ISNUMERIC(SUBSTRING(LTRIM(@ItemName), 1, 1)) = 1
		SET @errors = @errors + 'Item name cannot start with a number. ';
	IF @ItemDisplayID < 0
		SET @errors = @errors + 'Item display ID cannot be negative. ';
	IF @ItemQuality < 0
		SET @errors = @errors + 'Item quality cannot be negative. ';
	IF @ItemStackLimit < 1
		SET @errors = @errors + 'Item stack limit cannot be negative (or zero). ';
	IF @ItemBuyPrice < 0
		SET @errors = @errors + 'Item buyprice cannot be negative. ';
	IF @ItemSellPrice < 0
		SET @errors = @errors + 'Item sellprice cannot be negative. ';
	IF @ItemLevel < 0
		SET @errors = @errors + 'Item level cannot be negative. ';
	IF @ItemRequiredLevel < 1
		SET @errors = @errors + 'Item required level cannot be negative (or zero).';
	
	RETURN @errors;		
END;
GO

--------------------------------------------------------------
-- ITEM DROP VALIDATOR
--------------------------------------------------------------

IF OBJECT_ID (N'dbo.ufn_validate_item_drop', N'FN') IS NOT NULL
    DROP FUNCTION ufn_validate_item_drop;
GO

CREATE FUNCTION ufn_validate_item_drop
(
	@ItemID        INT,
	@CreatureID    INT,
	@ItemDroprate  FLOAT,
	@ItemDropcount INT
)
RETURNS VARCHAR(510)
AS
BEGIN
	DECLARE @errors VARCHAR(510);
	SET @errors = '';

	IF EXISTS (SELECT * FROM item_drop
			WHERE ItemEntryID = @ItemID AND CreatureEntryID = @CreatureID)
		RETURN 'Item drop already exists';
	IF @ItemDroprate < 0
		SET @errors = 'Item droprate cannot be negative. ';
	IF @ItemDropcount < 1
		SET @errors = @errors + 'Item dropcount cannot be negative (or zero).';

	RETURN @errors;
END;
GO

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--																				MANY-TO-MANY INSERTION SPs
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------
-- ROLLBACK TO BEGINNING OF TRANSACTION
--------------------------------------------------------------

IF OBJECT_ID(N'dbo.usp_create_item_drop_no_recover') IS NOT NULL
	DROP PROCEDURE usp_create_item_drop_no_recover
GO

CREATE PROCEDURE usp_create_item_drop_no_recover
	@CreatureName      VARCHAR(255),
	@CreatureSubname   VARCHAR(255),
	@CreatureMinLevel  INT,
	@CreatureMaxLevel  INT,
	@Faction           INT,
	@CreatureType      INT,
	@ItemName          VARCHAR(255),
	@ItemDisplayID     INT,
	@ItemQuality       INT,
	@ItemStackLimit    INT,
	@ItemBuyPrice      INT,
	@ItemSellPrice     INT,
	@ItemLevel         INT,
	@ItemRequiredLevel INT,
	@ItemDropcount     INT,
	@ItemDroprate      FLOAT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @validate_res VARCHAR(510);
	DECLARE @start DATETIME;
	DECLARE @end DATETIME;
	DECLARE @tableid INT;
	DECLARE @actionid INT;
	DECLARE @itemid INT;
	DECLARE @creatureid INT;

	BEGIN TRY
		BEGIN TRAN

		IF NOT EXISTS (SELECT * FROM tablelog 
				WHERE TableName = 'dbo.creature')
			INSERT INTO tablelog (TableName) VALUES ('dbo.creature');
		SELECT @tableid = TableID FROM tablelog WHERE TableName = 'dbo.creature';
		--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		--																				validate data
		--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		---------------------
		-- validate creature
		---------------------
		SET @start = SYSDATETIME();
		SELECT @validate_res = dbo.ufn_validate_creature(@CreatureName, @CreatureSubname, @CreatureMinLevel, @CreatureMaxLevel, @Faction, @CreatureType)
		SET @end = SYSDATETIME();
		IF @validate_res != ''
		BEGIN
			SET @validate_res = 'Error(s) validating creature: ' + @validate_res;
			THROW 51337, @validate_res, 1;
		END
		INSERT INTO actionlog (ActionName, StartAt, EndAt) VALUES ('test creature fields', @start, @end);
		SELECT @actionid = ActionID FROM actionlog WHERE ActionName = 'test creature fields' AND EndAt = @end;
		INSERT INTO tableactionlog (TableID, ActionID) VALUES (@tableid, @actionid);

		---------------------
		-- validate item
		---------------------
		IF NOT EXISTS (SELECT * FROM tablelog
				WHERE TableName = 'dbo.item')
			INSERT INTO tablelog (TableName) VALUES ('dbo.item');
		SELECT @tableid = TableID FROM tablelog WHERE TableName = 'dbo.item';
		SET @start = SYSDATETIME();
		SELECT @validate_res = dbo.ufn_validate_item(@ItemName, @ItemDisplayID, @ItemQuality, @ItemStackLimit, @ItemBuyPrice, @ItemSellPrice, @ItemLevel, @ItemRequiredLevel);
		SET @end = SYSDATETIME();
		IF @validate_res != ''
		BEGIN
			SET @validate_res = 'Error(s) validating item: ' + @validate_res;
			THROW 51338, @validate_res, 1;
		END
		INSERT INTO actionlog (ActionName, StartAt, EndAt) VALUES ('test item fields', @start, @end);
		SELECT @actionid = ActionID FROM actionlog WHERE ActionName = 'test item fields' AND EndAt = @end;
		INSERT INTO tableactionlog (TableID, ActionID) VALUES (@tableid, @actionid);

		--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		--																				insert data
		--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		--------------------
		-- insert creature
		--------------------
		IF NOT EXISTS(SELECT * FROM creature 
				WHERE Name = @CreatureName AND Subname = @CreatureSubname)
		BEGIN
			SET @start = SYSDATETIME();
			INSERT INTO creature(Name, Subname, MinLevel, MaxLevel, Faction, CreatureType) VALUES (@CreatureName, @CreatureSubname, @CreatureMinLevel, @CreatureMaxLevel, @Faction, @CreatureType);
			SET @end = SYSDATETIME();
			SELECT @tableid = TableID FROM tablelog WHERE TableName = 'dbo.creature';
			INSERT INTO actionlog(ActionName, StartAt, EndAt) VALUES ('insert creature', @start, @end);
			SELECT @actionid = ActionID FROM actionlog WHERE ActionName = 'insert creature' AND EndAt = @end;
			INSERT INTO tableactionlog (TableID, ActionID) VALUES (@tableid, @actionid);
		END

		---------------------
		-- insert item
		---------------------
		IF NOT EXISTS(SELECT * FROM item 
				WHERE Name = @ItemName AND DisplayID = @ItemDisplayID)
		BEGIN
			SET @start = SYSDATETIME();
			INSERT INTO item(Name, DisplayID, Quality, StackLimit, BuyPrice, SellPrice, ItemLevel, RequiredLevel) VALUES (@ItemName, @ItemDisplayID, @ItemQuality, @ItemStackLimit, @ItemBuyPrice, @ItemSellPrice, @ItemLevel, @ItemRequiredLevel);
			SET @end = SYSDATETIME();
			SELECT @tableid = TableID FROM tablelog WHERE TableName = 'dbo.item';
			INSERT INTO actionlog(ActionName, StartAt, EndAt) VALUES ('insert item', @start, @end);
			SELECT @actionid = ActionID FROM actionlog WHERE ActionName = 'insert item' AND EndAt = @end;
			INSERT INTO tableactionlog (TableID, ActionID) VALUES (@tableid, @actionid);
		END

		--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		--                                                                            crosstable section
		--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        ---------------------
        -- validate item drop
        ---------------------
		IF NOT EXISTS (SELECT * FROM tablelog 
				WHERE TableName = 'dbo.item_drop')
			INSERT INTO tablelog (TableName) VALUES ('dbo.item_drop');
		SELECT @tableid = @TableID FROM tablelog WHERE TableName = 'dbo.item_drop';
		SET @start = SYSDATETIME();
		SELECT @itemid = EntryID FROM item WHERE Name = @ItemName AND DisplayID = @ItemDisplayID;
		SELECT @creatureid = EntryID FROM creature WHERE Name = @CreatureName AND Subname = @CreatureSubname;
		SELECT @validate_res = dbo.ufn_validate_item_drop(@itemid, @creatureid, @ItemDroprate, @Itemdropcount);
		SET @end = SYSDATETIME();
		IF @validate_res != ''
		BEGIN
			SET @validate_res = 'Error(s) validating item_drop: ' + @validate_res;
			THROW 51339, @validate_res, 1;
		END
		INSERT INTO actionlog(ActionName, StartAt, EndAt) VALUES ('test item_drop fields', @start, @end);
		SELECT @actionid = ActionID FROM actionlog WHERE ActionName = 'test item_drop fields' AND EndAt = @end;
		INSERT INTO tableactionlog (TableID, ActionID) VALUES (@tableid, @actionid);

		--------------------
		-- insert item drop
		--------------------
		SET @start = SYSDATETIME();
		INSERT INTO item_drop(ItemEntryID, CreatureEntryID, Dropcount, Droprate) VALUES (@itemid, @creatureid, @ItemDropcount, @ItemDroprate);
		SET @end = SYSDATETIME();
		SELECT @tableid = TableID FROM tablelog WHERE TableName = 'dbo.item_drop';
		INSERT INTO actionlog(ActionName, StartAt, EndAt) VALUES ('insert item_drop', @start, @end);
		SELECT @actionid = ActionID FROM actionlog WHERE ActionName = 'insert item_drop' AND EndAt = @end;
		INSERT INTO tableactionlog (TableID, ActionID) VALUES (@tableid, @actionid);

		COMMIT TRAN
	END TRY

	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK

		SELECT ERROR_NUMBER() AS [Error number]
        , cast(ERROR_NUMBER() as varchar(20)) + ', line: '
        + cast(ERROR_LINE() as varchar(20)) + '. ' 
        + ERROR_MESSAGE() + ' Thrown by: ' 
        + ERROR_PROCEDURE() AS [Error message]
		
		RETURN ERROR_NUMBER();
	END CATCH
END;
GO

--------------------------------------------------------------
-- ROLLBACK ONLY LAST UNSUCCESSFUL OPERATION
--------------------------------------------------------------

IF OBJECT_ID(N'dbo.usp_create_item_drop_recover') IS NOT NULL
	DROP PROCEDURE usp_create_item_drop_recover
GO

CREATE PROCEDURE usp_create_item_drop_recover
	@CreatureName      VARCHAR(255),
	@CreatureSubname   VARCHAR(255),
	@CreatureMinLevel  INT,
	@CreatureMaxLevel  INT,
	@Faction           INT,
	@CreatureType      INT,
	@ItemName          VARCHAR(255),
	@ItemDisplayID     INT,
	@ItemQuality       INT,
	@ItemStackLimit    INT,
	@ItemBuyPrice      INT,
	@ItemSellPrice     INT,
	@ItemLevel         INT,
	@ItemRequiredLevel INT,
	@ItemDropcount     INT,
	@ItemDroprate      FLOAT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @validate_res VARCHAR(510);
	DECLARE @start DATETIME;
	DECLARE @end DATETIME;
	DECLARE @tableid INT;
	DECLARE @actionid INT;
	DECLARE @itemid INT;
	DECLARE @creatureid INT;

	BEGIN TRY
		--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		--																				create creature
		--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		BEGIN TRAN

		IF NOT EXISTS (SELECT * FROM tablelog 
				WHERE TableName = 'dbo.creature')
			INSERT INTO tablelog (TableName) VALUES ('dbo.creature');
		SELECT @tableid = TableID FROM tablelog WHERE TableName = 'dbo.creature';

		---------------------
		-- validate creature
		---------------------
		SET @start = SYSDATETIME();
		SELECT @validate_res = dbo.ufn_validate_creature(@CreatureName, @CreatureSubname, @CreatureMinLevel, @CreatureMaxLevel, @Faction, @CreatureType)
		SET @end = SYSDATETIME();
		IF @validate_res != ''
		BEGIN
			SET @validate_res = 'Error(s) validating creature: ' + @validate_res;
			THROW 51337, @validate_res, 1;
		END
		INSERT INTO actionlog (ActionName, StartAt, EndAt) VALUES ('test creature fields', @start, @end);
		SELECT @actionid = ActionID FROM actionlog WHERE ActionName = 'test creature fields' AND EndAt = @end;
		INSERT INTO tableactionlog (TableID, ActionID) VALUES (@tableid, @actionid);

		--------------------
		-- insert creature
		--------------------
		IF NOT EXISTS(SELECT * FROM creature 
				WHERE Name = @CreatureName AND Subname = @CreatureSubname)
		BEGIN
			SET @start = SYSDATETIME();
			INSERT INTO creature(Name, Subname, MinLevel, MaxLevel, Faction, CreatureType) VALUES (@CreatureName, @CreatureSubname, @CreatureMinLevel, @CreatureMaxLevel, @Faction, @CreatureType);
			SET @end = SYSDATETIME();
			SELECT @tableid = TableID FROM tablelog WHERE TableName = 'dbo.creature';
			INSERT INTO actionlog(ActionName, StartAt, EndAt) VALUES ('insert creature', @start, @end);
			SELECT @actionid = ActionID FROM actionlog WHERE ActionName = 'insert creature' AND EndAt = @end;
			INSERT INTO tableactionlog (TableID, ActionID) VALUES (@tableid, @actionid);
		END

		COMMIT TRAN

		--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		--																				create item
		--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		BEGIN TRAN
		
		IF NOT EXISTS (SELECT * FROM tablelog
				WHERE TableName = 'dbo.item')
			INSERT INTO tablelog (TableName) VALUES ('dbo.item');
		SELECT @tableid = TableID FROM tablelog WHERE TableName = 'dbo.item';

		---------------------
		-- validate item
		---------------------
		SET @start = SYSDATETIME();
		SELECT @validate_res = dbo.ufn_validate_item(@ItemName, @ItemDisplayID, @ItemQuality, @ItemStackLimit, @ItemBuyPrice, @ItemSellPrice, @ItemLevel, @ItemRequiredLevel);
		SET @end = SYSDATETIME();
		IF @validate_res != ''
		BEGIN
			SET @validate_res = 'Error(s) validating item: ' + @validate_res;
			THROW 51338, @validate_res, 1;
		END
		INSERT INTO actionlog (ActionName, StartAt, EndAt) VALUES ('test item fields', @start, @end);
		SELECT @actionid = ActionID FROM actionlog WHERE ActionName = 'test item fields' AND EndAt = @end;
		INSERT INTO tableactionlog (TableID, ActionID) VALUES (@tableid, @actionid);

		--------------------
		-- insert item
		--------------------
		IF NOT EXISTS(SELECT * FROM item 
				WHERE Name = @ItemName AND DisplayID = @ItemDisplayID)
		BEGIN
			SET @start = SYSDATETIME();
			INSERT INTO item(Name, DisplayID, Quality, StackLimit, BuyPrice, SellPrice, ItemLevel, RequiredLevel) VALUES (@ItemName, @ItemDisplayID, @ItemQuality, @ItemStackLimit, @ItemBuyPrice, @ItemSellPrice, @ItemLevel, @ItemRequiredLevel);
			SET @end = SYSDATETIME();
			SELECT @tableid = TableID FROM tablelog WHERE TableName = 'dbo.item';
			INSERT INTO actionlog(ActionName, StartAt, EndAt) VALUES ('insert item', @start, @end);
			SELECT @actionid = ActionID FROM actionlog WHERE ActionName = 'insert item' AND EndAt = @end;
			INSERT INTO tableactionlog (TableID, ActionID) VALUES (@tableid, @actionid);
		END

		COMMIT TRAN
		--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		--																				create item drop
		--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		BEGIN TRAN
		
		IF NOT EXISTS (SELECT * FROM tablelog 
				WHERE TableName = 'dbo.item_drop')
			INSERT INTO tablelog (TableName) VALUES ('dbo.item_drop');
		SELECT @tableid = @TableID FROM tablelog WHERE TableName = 'dbo.item_drop';	
		---------------------
		-- validate item drop
		---------------------
		SET @start = SYSDATETIME();
		SELECT @itemid = EntryID FROM item WHERE Name = @ItemName AND DisplayID = @ItemDisplayID;
		SELECT @creatureid = EntryID FROM creature WHERE Name = @CreatureName AND Subname = @CreatureSubname;
		SELECT @validate_res = dbo.ufn_validate_item_drop(@itemid, @creatureid, @ItemDroprate, @Itemdropcount);
		SET @end = SYSDATETIME();
		IF @validate_res != ''
		BEGIN
			SET @validate_res = 'Error(s) validating item_drop: ' + @validate_res;
			THROW 51339, @validate_res, 1;
		END
		INSERT INTO actionlog(ActionName, StartAt, EndAt) VALUES ('test item_drop fields', @start, @end);
		SELECT @actionid = ActionID FROM actionlog WHERE ActionName = 'test item_drop fields' AND EndAt = @end;
		INSERT INTO tableactionlog (TableID, ActionID) VALUES (@tableid, @actionid);

		--------------------
		-- insert item drop
		--------------------
		SET @start = SYSDATETIME();
		INSERT INTO item_drop(ItemEntryID, CreatureEntryID, Dropcount, Droprate) VALUES (@itemid, @creatureid, @ItemDropcount, @ItemDroprate);
		SET @end = SYSDATETIME();
		SELECT @tableid = TableID FROM tablelog WHERE TableName = 'dbo.item_drop';
		INSERT INTO actionlog(ActionName, StartAt, EndAt) VALUES ('insert item_drop', @start, @end);
		SELECT @actionid = ActionID FROM actionlog WHERE ActionName = 'insert item_drop' AND EndAt = @end;
		INSERT INTO tableactionlog (TableID, ActionID) VALUES (@tableid, @actionid);

		COMMIT TRAN
	END TRY

	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK

		SELECT ERROR_NUMBER() AS [Error number]
        , cast(ERROR_NUMBER() as varchar(20)) + ', line: '
        + cast(ERROR_LINE() as varchar(20)) + '. ' 
        + ERROR_MESSAGE() + ' Thrown by: ' 
        + ERROR_PROCEDURE() AS [Error message]
		
		RETURN ERROR_NUMBER();
	END CATCH
END;
GO

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--																							MISC
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- EXECUTE dbo.usp_create_item_drop_recover 'asdf', 'Lich King', 80, 80, 1156, 1, 'name', 55758, 6, 1, 40, 20, 277, 80, 1, 0.01;