USE TempWDB
GO

-- EXECUTE dbo.usp_create_item_drop_recover 'Arthas', 'Prince of Menethil', 80, 80, 1156, 1, 'name', 55758, 6, 1, 40, 20, 277, 80, 1, 0.01;

BEGIN TRAN;
	EXECUTE dbo.usp_create_item_drop_no_recover 'Garrosh Hellscream', 'Orc chieftain', 80, 100, 5, 1, 'Gorehowl', 33756, 5, 1, 500, 200, 156, 70, 1, 0.1;
COMMIT TRAN;

SELECT * FROM item_drop;