USE TempWDB
GO

---- SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

--BEGIN TRAN;
SELECT * FROM item_drop --WITH(TABLOCK, HOLDLOCK)
	WHERE ItemEntryID > 1 AND ItemEntryID < 10;

WAITFOR DELAY '00:00:05';

SELECT * FROM item_drop
	WHERE ItemEntryID > 1 AND ItemEntryID < 10;

--COMMIT TRAN;