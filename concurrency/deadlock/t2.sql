USE TempWDB
GO

BEGIN TRAN;

UPDATE item SET BuyPrice = 50010 WHERE Name = 'Frostmourne';

WAITFOR DELAY '00:00:05';

UPDATE creature SET Name = 'Hodor' WHERE Subname = 'Lich King';

COMMIT TRAN;