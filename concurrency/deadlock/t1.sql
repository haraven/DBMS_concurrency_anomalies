USE TempWDB
GO


BEGIN TRAN;

UPDATE creature SET Name = 'Ner''zhul' WHERE Subname = 'Lich King';

WAITFOR DELAY '00:00:05';

UPDATE item SET BuyPrice = 50000 WHERE Name = 'Frostmourne';

COMMIT TRAN;