USE TempWDB
GO

---- SET TRANSACTION ISOLATION LEVEL REPEATABLE READ;
SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

-- BEGIN TRAN;
SELECT * FROM creature --WITH(TABLOCK, HOLDLOCK)
	WHERE Name = 'Arthas' AND Subname = 'Prince of Menethil';

WAITFOR DELAY '00:00:05';

SELECT * FROM creature
	WHERE Name = 'Arthas' AND Subname = 'Prince of Menethil';

--COMMIT TRAN;