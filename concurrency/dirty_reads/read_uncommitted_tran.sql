USE TempWDB

---- SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @NextExpansionLevel INT;

SELECT @NextExpansionLevel = MinLevel FROM creature
	WHERE Name = 'Arthas' AND Subname = 'Prince Of Menethil';

IF @NextExpansionLevel IS NOT NULL
	BEGIN
		SET @NextExpansionLevel = @NextExpansionLevel + 5;
		PRINT @NextExpansionLevel;
	END
ELSE
	PRINT 'No entry with the name ''asdf'' found.';