USE TempWDB
GO

-- EXECUTE dbo.usp_create_item_drop_recover 'Arthas', 'Prince of Menethil', 80, 80, 1156, 1, 'name', 55758, 6, 1, 40, 20, 277, 80, 1, 0.01;

BEGIN TRAN;
IF EXISTS(SELECT * FROM creature 
	WHERE Name = 'Arthas' AND Subname = 'Prince of Menethil')
	UPDATE creature SET MinLevel = 30
		WHERE Name = 'Arthas' AND Subname = 'Prince of Menethil';

WAITFOR DELAY '00:00:5';
ROLLBACK TRAN;

SELECT * FROM creature
	WHERE Name = 'Arthas' AND Subname = 'Prince of Menethil';