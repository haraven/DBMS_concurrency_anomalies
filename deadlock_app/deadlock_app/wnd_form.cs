﻿using System;
using System.Data.SqlClient;
using System.Threading;
using System.Windows.Forms;

namespace deadlock_app
{
    public partial class wnd_form : Form
    {
        public wnd_form()
        {
            InitializeComponent();
            UpdateRetries();
            retries = decimal.ToInt32(retry_input.Value);
            UpdateOperationStatus("idle");
            UpdateTransactionStatus("none");

            FillGrids();
        }

        private void FillGrids()
        {
            creature_table_adapter.Fill(temp_wdb_data_set.creature);
            item_table_adapter.Fill(temp_wdb_data_set.item);
        }

        private void UpdateRetries()
        {
            retries_left_status.Text = "Retries left: " + retries.ToString();
        }

        private void UpdateOperationStatus(string new_status)
        {
            operation_status.Text = "Operation status: " + new_status + '.';
        }

        private void UpdateTransactionStatus(string new_status)
        {
            crt_transaction_status.Text = "Executing: " + new_status + '.';
        }

        private void OnRetryCountChanged(object sender, EventArgs e)
        {
            retries = decimal.ToInt32(retry_input.Value);
            UpdateRetries();
        }

        private void DisableButtons()
        {
            t1_btn.Enabled = t2_btn.Enabled = false;
        }

        private void EnableButtons()
        {
            t1_btn.Enabled = t2_btn.Enabled = true;
        }

        private void OnT1Clicked(object sender, EventArgs e)
        {
            DisableButtons();
            UpdateTransactionStatus("T1");
            UpdateOperationStatus("running");

            string tran_status = "none";
            string op_status = "success";

            try
            {
                SqlConnection conn = creature_table_adapter.Connection;
                item_table_adapter.Connection = conn;
                conn.Open();
                SqlTransaction tran = creature_table_adapter.Connection.BeginTransaction();
                item_table_adapter.Transaction = tran;
                creature_table_adapter.Transaction = tran;

                creature_table_adapter.T1UpdateCreature();
                Thread.Sleep(5000);
                item_table_adapter.T2UpdateItem();

                tran.Commit();
                conn.Close();
            }
            catch (SqlException ex)
            {
                if (ex.Number == 1205)
                {
                    if (item_table_adapter.Connection.State != System.Data.ConnectionState.Closed)
                    {
                        item_table_adapter.Connection.Close();
                    }

                    if (retries > 0)
                    {
                        --retries;
                        UpdateRetries();
                        OnT2Clicked(sender, e);
                    }
                    else
                    {
                        tran_status = "T1 deadlocked";
                        op_status = "failed";
                    }
                }
                else throw;
            }

            EnableButtons();
            if (op_status.Equals("success"))
                FillGrids();
            UpdateTransactionStatus(tran_status);
            UpdateOperationStatus(op_status);
        }

        private void OnT2Clicked(object sender, EventArgs e)
        {
            DisableButtons();
            UpdateTransactionStatus("T2");
            UpdateOperationStatus("running");

            string tran_status = "none";
            string op_status   = "success";

            try
            {
                SqlConnection conn = item_table_adapter.Connection;
                creature_table_adapter.Connection = conn;
                conn.Open();
                SqlTransaction tran = item_table_adapter.Connection.BeginTransaction();
                item_table_adapter.Transaction = tran;
                creature_table_adapter.Transaction = tran;

                item_table_adapter.T2UpdateItem();
                Thread.Sleep(5000);
                creature_table_adapter.T2UpdateCreature();

                tran.Commit();
                conn.Close();
            }
            catch (SqlException ex)
            {
                if (ex.Number == 1205)
                {
                    if (item_table_adapter.Connection.State != System.Data.ConnectionState.Closed)
                    {
                        item_table_adapter.Connection.Close();
                    }

                    if (retries > 0)
                    {
                        --retries;
                        UpdateRetries();
                        OnT2Clicked(sender, e);
                    }
                    else
                    {
                        tran_status = "T2 deadlocked";
                        op_status = "failed";
                    }
                }
                else throw;
            }

            EnableButtons();
            if (op_status.Equals("success"))
                FillGrids();
            UpdateTransactionStatus(tran_status);
            UpdateOperationStatus(op_status);
        }

        private int retries;
    }
}
