﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace deadlock_app
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Thread thread1 = new Thread(new ThreadStart(() =>
            {
                var form1 = new wnd_form();
                form1.ShowDialog();
            }));
            Thread thread2 = new Thread(new ThreadStart(() =>
            {
                var form2 = new wnd_form();
                form2.ShowDialog();
            }));
            thread1.Start();
            thread2.Start();

            thread1.Join();
            thread2.Join();
        }
    }
}
