﻿namespace deadlock_app
{
    partial class wnd_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.main_layout = new System.Windows.Forms.TableLayoutPanel();
            this.status_strip = new System.Windows.Forms.StatusStrip();
            this.operation_status = new System.Windows.Forms.ToolStripStatusLabel();
            this.separator = new System.Windows.Forms.ToolStripStatusLabel();
            this.crt_transaction_status = new System.Windows.Forms.ToolStripStatusLabel();
            this.empty_status = new System.Windows.Forms.ToolStripStatusLabel();
            this.retries_left_status = new System.Windows.Forms.ToolStripStatusLabel();
            this.buttons_and_status_layout = new System.Windows.Forms.TableLayoutPanel();
            this.grids_panel = new System.Windows.Forms.TableLayoutPanel();
            this.item_grid = new System.Windows.Forms.DataGridView();
            this.entryIDDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.displayIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qualityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stackLimitDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buyPriceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sellPriceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemLevelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.requiredLevelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.temp_wdb_data_set = new deadlock_app.TempWDBDataSet();
            this.creature_grid = new System.Windows.Forms.DataGridView();
            this.entryIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.minLevelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maxLevelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.factionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.creatureTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.creatureBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.buttons_panel = new System.Windows.Forms.TableLayoutPanel();
            this.t2_btn = new System.Windows.Forms.Button();
            this.t1_btn = new System.Windows.Forms.Button();
            this.retries_layout = new System.Windows.Forms.TableLayoutPanel();
            this.retries_label = new System.Windows.Forms.Label();
            this.retry_input = new System.Windows.Forms.NumericUpDown();
            this.creature_table_adapter = new deadlock_app.TempWDBDataSetTableAdapters.creatureTableAdapter();
            this.item_table_adapter = new deadlock_app.TempWDBDataSetTableAdapters.itemTableAdapter();
            this.main_layout.SuspendLayout();
            this.status_strip.SuspendLayout();
            this.buttons_and_status_layout.SuspendLayout();
            this.grids_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.item_grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.temp_wdb_data_set)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.creature_grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.creatureBindingSource)).BeginInit();
            this.buttons_panel.SuspendLayout();
            this.retries_layout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.retry_input)).BeginInit();
            this.SuspendLayout();
            // 
            // main_layout
            // 
            this.main_layout.ColumnCount = 1;
            this.main_layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.main_layout.Controls.Add(this.status_strip, 0, 2);
            this.main_layout.Controls.Add(this.buttons_and_status_layout, 0, 1);
            this.main_layout.Controls.Add(this.retries_layout, 0, 0);
            this.main_layout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_layout.Location = new System.Drawing.Point(0, 0);
            this.main_layout.Name = "main_layout";
            this.main_layout.RowCount = 3;
            this.main_layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.252747F));
            this.main_layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 92.74725F));
            this.main_layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.main_layout.Size = new System.Drawing.Size(1353, 483);
            this.main_layout.TabIndex = 0;
            // 
            // status_strip
            // 
            this.status_strip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.status_strip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.operation_status,
            this.separator,
            this.crt_transaction_status,
            this.empty_status,
            this.retries_left_status});
            this.status_strip.Location = new System.Drawing.Point(0, 455);
            this.status_strip.Name = "status_strip";
            this.status_strip.Size = new System.Drawing.Size(1353, 28);
            this.status_strip.TabIndex = 3;
            this.status_strip.Text = "Status:";
            // 
            // operation_status
            // 
            this.operation_status.Name = "operation_status";
            this.operation_status.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.operation_status.Size = new System.Drawing.Size(97, 23);
            this.operation_status.Text = "Operation status:";
            // 
            // separator
            // 
            this.separator.Name = "separator";
            this.separator.Size = new System.Drawing.Size(10, 23);
            this.separator.Text = "|";
            // 
            // crt_transaction_status
            // 
            this.crt_transaction_status.Name = "crt_transaction_status";
            this.crt_transaction_status.Size = new System.Drawing.Size(61, 23);
            this.crt_transaction_status.Text = "Executing:";
            // 
            // empty_status
            // 
            this.empty_status.Name = "empty_status";
            this.empty_status.Size = new System.Drawing.Size(442, 23);
            this.empty_status.Spring = true;
            // 
            // retries_left_status
            // 
            this.retries_left_status.Name = "retries_left_status";
            this.retries_left_status.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.retries_left_status.Size = new System.Drawing.Size(65, 23);
            this.retries_left_status.Text = "Retries left:";
            // 
            // buttons_and_status_layout
            // 
            this.buttons_and_status_layout.ColumnCount = 1;
            this.buttons_and_status_layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.buttons_and_status_layout.Controls.Add(this.grids_panel, 0, 0);
            this.buttons_and_status_layout.Controls.Add(this.buttons_panel, 0, 1);
            this.buttons_and_status_layout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttons_and_status_layout.Location = new System.Drawing.Point(3, 36);
            this.buttons_and_status_layout.Name = "buttons_and_status_layout";
            this.buttons_and_status_layout.RowCount = 2;
            this.buttons_and_status_layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.buttons_and_status_layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.buttons_and_status_layout.Size = new System.Drawing.Size(1347, 416);
            this.buttons_and_status_layout.TabIndex = 0;
            // 
            // grids_panel
            // 
            this.grids_panel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.grids_panel.ColumnCount = 2;
            this.grids_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.grids_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.grids_panel.Controls.Add(this.item_grid, 1, 0);
            this.grids_panel.Controls.Add(this.creature_grid, 0, 0);
            this.grids_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grids_panel.Location = new System.Drawing.Point(3, 3);
            this.grids_panel.Name = "grids_panel";
            this.grids_panel.RowCount = 1;
            this.grids_panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.grids_panel.Size = new System.Drawing.Size(1341, 355);
            this.grids_panel.TabIndex = 4;
            // 
            // item_grid
            // 
            this.item_grid.AllowUserToAddRows = false;
            this.item_grid.AllowUserToDeleteRows = false;
            this.item_grid.AutoGenerateColumns = false;
            this.item_grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.item_grid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.item_grid.BackgroundColor = System.Drawing.Color.Brown;
            this.item_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.item_grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.entryIDDataGridViewTextBoxColumn1,
            this.nameDataGridViewTextBoxColumn1,
            this.displayIDDataGridViewTextBoxColumn,
            this.qualityDataGridViewTextBoxColumn,
            this.stackLimitDataGridViewTextBoxColumn,
            this.buyPriceDataGridViewTextBoxColumn,
            this.sellPriceDataGridViewTextBoxColumn,
            this.itemLevelDataGridViewTextBoxColumn,
            this.requiredLevelDataGridViewTextBoxColumn});
            this.item_grid.DataSource = this.itemBindingSource;
            this.item_grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.item_grid.Location = new System.Drawing.Point(673, 3);
            this.item_grid.Name = "item_grid";
            this.item_grid.ReadOnly = true;
            this.item_grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.item_grid.Size = new System.Drawing.Size(665, 349);
            this.item_grid.TabIndex = 1;
            // 
            // entryIDDataGridViewTextBoxColumn1
            // 
            this.entryIDDataGridViewTextBoxColumn1.DataPropertyName = "EntryID";
            this.entryIDDataGridViewTextBoxColumn1.HeaderText = "EntryID";
            this.entryIDDataGridViewTextBoxColumn1.Name = "entryIDDataGridViewTextBoxColumn1";
            this.entryIDDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // nameDataGridViewTextBoxColumn1
            // 
            this.nameDataGridViewTextBoxColumn1.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn1.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn1.Name = "nameDataGridViewTextBoxColumn1";
            this.nameDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // displayIDDataGridViewTextBoxColumn
            // 
            this.displayIDDataGridViewTextBoxColumn.DataPropertyName = "DisplayID";
            this.displayIDDataGridViewTextBoxColumn.HeaderText = "DisplayID";
            this.displayIDDataGridViewTextBoxColumn.Name = "displayIDDataGridViewTextBoxColumn";
            this.displayIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // qualityDataGridViewTextBoxColumn
            // 
            this.qualityDataGridViewTextBoxColumn.DataPropertyName = "Quality";
            this.qualityDataGridViewTextBoxColumn.HeaderText = "Quality";
            this.qualityDataGridViewTextBoxColumn.Name = "qualityDataGridViewTextBoxColumn";
            this.qualityDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // stackLimitDataGridViewTextBoxColumn
            // 
            this.stackLimitDataGridViewTextBoxColumn.DataPropertyName = "StackLimit";
            this.stackLimitDataGridViewTextBoxColumn.HeaderText = "StackLimit";
            this.stackLimitDataGridViewTextBoxColumn.Name = "stackLimitDataGridViewTextBoxColumn";
            this.stackLimitDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // buyPriceDataGridViewTextBoxColumn
            // 
            this.buyPriceDataGridViewTextBoxColumn.DataPropertyName = "BuyPrice";
            this.buyPriceDataGridViewTextBoxColumn.HeaderText = "BuyPrice";
            this.buyPriceDataGridViewTextBoxColumn.Name = "buyPriceDataGridViewTextBoxColumn";
            this.buyPriceDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // sellPriceDataGridViewTextBoxColumn
            // 
            this.sellPriceDataGridViewTextBoxColumn.DataPropertyName = "SellPrice";
            this.sellPriceDataGridViewTextBoxColumn.HeaderText = "SellPrice";
            this.sellPriceDataGridViewTextBoxColumn.Name = "sellPriceDataGridViewTextBoxColumn";
            this.sellPriceDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // itemLevelDataGridViewTextBoxColumn
            // 
            this.itemLevelDataGridViewTextBoxColumn.DataPropertyName = "ItemLevel";
            this.itemLevelDataGridViewTextBoxColumn.HeaderText = "ItemLevel";
            this.itemLevelDataGridViewTextBoxColumn.Name = "itemLevelDataGridViewTextBoxColumn";
            this.itemLevelDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // requiredLevelDataGridViewTextBoxColumn
            // 
            this.requiredLevelDataGridViewTextBoxColumn.DataPropertyName = "RequiredLevel";
            this.requiredLevelDataGridViewTextBoxColumn.HeaderText = "RequiredLevel";
            this.requiredLevelDataGridViewTextBoxColumn.Name = "requiredLevelDataGridViewTextBoxColumn";
            this.requiredLevelDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // itemBindingSource
            // 
            this.itemBindingSource.DataMember = "item";
            this.itemBindingSource.DataSource = this.temp_wdb_data_set;
            // 
            // temp_wdb_data_set
            // 
            this.temp_wdb_data_set.DataSetName = "TempWDBDataSet";
            this.temp_wdb_data_set.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // creature_grid
            // 
            this.creature_grid.AllowUserToAddRows = false;
            this.creature_grid.AllowUserToDeleteRows = false;
            this.creature_grid.AutoGenerateColumns = false;
            this.creature_grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.creature_grid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.creature_grid.BackgroundColor = System.Drawing.Color.Brown;
            this.creature_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.creature_grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.entryIDDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.subnameDataGridViewTextBoxColumn,
            this.minLevelDataGridViewTextBoxColumn,
            this.maxLevelDataGridViewTextBoxColumn,
            this.factionDataGridViewTextBoxColumn,
            this.creatureTypeDataGridViewTextBoxColumn});
            this.creature_grid.DataSource = this.creatureBindingSource;
            this.creature_grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.creature_grid.Location = new System.Drawing.Point(3, 3);
            this.creature_grid.Name = "creature_grid";
            this.creature_grid.ReadOnly = true;
            this.creature_grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.creature_grid.Size = new System.Drawing.Size(664, 349);
            this.creature_grid.TabIndex = 0;
            // 
            // entryIDDataGridViewTextBoxColumn
            // 
            this.entryIDDataGridViewTextBoxColumn.DataPropertyName = "EntryID";
            this.entryIDDataGridViewTextBoxColumn.HeaderText = "EntryID";
            this.entryIDDataGridViewTextBoxColumn.Name = "entryIDDataGridViewTextBoxColumn";
            this.entryIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // subnameDataGridViewTextBoxColumn
            // 
            this.subnameDataGridViewTextBoxColumn.DataPropertyName = "Subname";
            this.subnameDataGridViewTextBoxColumn.HeaderText = "Subname";
            this.subnameDataGridViewTextBoxColumn.Name = "subnameDataGridViewTextBoxColumn";
            this.subnameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // minLevelDataGridViewTextBoxColumn
            // 
            this.minLevelDataGridViewTextBoxColumn.DataPropertyName = "MinLevel";
            this.minLevelDataGridViewTextBoxColumn.HeaderText = "MinLevel";
            this.minLevelDataGridViewTextBoxColumn.Name = "minLevelDataGridViewTextBoxColumn";
            this.minLevelDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // maxLevelDataGridViewTextBoxColumn
            // 
            this.maxLevelDataGridViewTextBoxColumn.DataPropertyName = "MaxLevel";
            this.maxLevelDataGridViewTextBoxColumn.HeaderText = "MaxLevel";
            this.maxLevelDataGridViewTextBoxColumn.Name = "maxLevelDataGridViewTextBoxColumn";
            this.maxLevelDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // factionDataGridViewTextBoxColumn
            // 
            this.factionDataGridViewTextBoxColumn.DataPropertyName = "Faction";
            this.factionDataGridViewTextBoxColumn.HeaderText = "Faction";
            this.factionDataGridViewTextBoxColumn.Name = "factionDataGridViewTextBoxColumn";
            this.factionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // creatureTypeDataGridViewTextBoxColumn
            // 
            this.creatureTypeDataGridViewTextBoxColumn.DataPropertyName = "CreatureType";
            this.creatureTypeDataGridViewTextBoxColumn.HeaderText = "CreatureType";
            this.creatureTypeDataGridViewTextBoxColumn.Name = "creatureTypeDataGridViewTextBoxColumn";
            this.creatureTypeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // creatureBindingSource
            // 
            this.creatureBindingSource.DataMember = "creature";
            this.creatureBindingSource.DataSource = this.temp_wdb_data_set;
            // 
            // buttons_panel
            // 
            this.buttons_panel.ColumnCount = 2;
            this.buttons_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.buttons_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.buttons_panel.Controls.Add(this.t2_btn, 0, 0);
            this.buttons_panel.Controls.Add(this.t1_btn, 0, 0);
            this.buttons_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttons_panel.Location = new System.Drawing.Point(3, 364);
            this.buttons_panel.Name = "buttons_panel";
            this.buttons_panel.RowCount = 1;
            this.buttons_panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.buttons_panel.Size = new System.Drawing.Size(1341, 49);
            this.buttons_panel.TabIndex = 5;
            // 
            // t2_btn
            // 
            this.t2_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.t2_btn.Location = new System.Drawing.Point(673, 3);
            this.t2_btn.Name = "t2_btn";
            this.t2_btn.Size = new System.Drawing.Size(665, 43);
            this.t2_btn.TabIndex = 5;
            this.t2_btn.Text = "Execute T2";
            this.t2_btn.UseVisualStyleBackColor = true;
            this.t2_btn.Click += new System.EventHandler(this.OnT2Clicked);
            // 
            // t1_btn
            // 
            this.t1_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.t1_btn.Location = new System.Drawing.Point(3, 3);
            this.t1_btn.Name = "t1_btn";
            this.t1_btn.Size = new System.Drawing.Size(664, 43);
            this.t1_btn.TabIndex = 4;
            this.t1_btn.Text = "Execute T1";
            this.t1_btn.UseVisualStyleBackColor = true;
            this.t1_btn.Click += new System.EventHandler(this.OnT1Clicked);
            // 
            // retries_layout
            // 
            this.retries_layout.ColumnCount = 2;
            this.retries_layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 93.85965F));
            this.retries_layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.140351F));
            this.retries_layout.Controls.Add(this.retries_label, 0, 0);
            this.retries_layout.Controls.Add(this.retry_input, 1, 0);
            this.retries_layout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.retries_layout.Location = new System.Drawing.Point(3, 3);
            this.retries_layout.Name = "retries_layout";
            this.retries_layout.RowCount = 1;
            this.retries_layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.retries_layout.Size = new System.Drawing.Size(1347, 27);
            this.retries_layout.TabIndex = 4;
            // 
            // retries_label
            // 
            this.retries_label.AutoSize = true;
            this.retries_label.Dock = System.Windows.Forms.DockStyle.Right;
            this.retries_label.Location = new System.Drawing.Point(1218, 0);
            this.retries_label.Name = "retries_label";
            this.retries_label.Size = new System.Drawing.Size(43, 27);
            this.retries_label.TabIndex = 0;
            this.retries_label.Text = "Retries:";
            this.retries_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // retry_input
            // 
            this.retry_input.Dock = System.Windows.Forms.DockStyle.Fill;
            this.retry_input.Location = new System.Drawing.Point(1267, 3);
            this.retry_input.Name = "retry_input";
            this.retry_input.Size = new System.Drawing.Size(77, 20);
            this.retry_input.TabIndex = 1;
            this.retry_input.ValueChanged += new System.EventHandler(this.OnRetryCountChanged);
            // 
            // creature_table_adapter
            // 
            this.creature_table_adapter.ClearBeforeFill = true;
            // 
            // item_table_adapter
            // 
            this.item_table_adapter.ClearBeforeFill = true;
            // 
            // wnd_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1353, 483);
            this.Controls.Add(this.main_layout);
            this.Name = "wnd_form";
            this.Text = "Deadlocks\'R\'Us";
            this.main_layout.ResumeLayout(false);
            this.main_layout.PerformLayout();
            this.status_strip.ResumeLayout(false);
            this.status_strip.PerformLayout();
            this.buttons_and_status_layout.ResumeLayout(false);
            this.grids_panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.item_grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.temp_wdb_data_set)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.creature_grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.creatureBindingSource)).EndInit();
            this.buttons_panel.ResumeLayout(false);
            this.retries_layout.ResumeLayout(false);
            this.retries_layout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.retry_input)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel main_layout;
        private System.Windows.Forms.TableLayoutPanel buttons_and_status_layout;
        private System.Windows.Forms.StatusStrip status_strip;
        private System.Windows.Forms.ToolStripStatusLabel operation_status;
        private System.Windows.Forms.ToolStripStatusLabel retries_left_status;
        private System.Windows.Forms.TableLayoutPanel grids_panel;
        private System.Windows.Forms.TableLayoutPanel retries_layout;
        private System.Windows.Forms.Label retries_label;
        private System.Windows.Forms.NumericUpDown retry_input;
        private System.Windows.Forms.DataGridView creature_grid;
        private System.Windows.Forms.DataGridView item_grid;
        private TempWDBDataSet temp_wdb_data_set;
        private System.Windows.Forms.BindingSource creatureBindingSource;
        private TempWDBDataSetTableAdapters.creatureTableAdapter creature_table_adapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn entryIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn subnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn minLevelDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn maxLevelDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn factionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn creatureTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource itemBindingSource;
        private TempWDBDataSetTableAdapters.itemTableAdapter item_table_adapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn entryIDDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn displayIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn qualityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn stackLimitDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn buyPriceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sellPriceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemLevelDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn requiredLevelDataGridViewTextBoxColumn;
        private System.Windows.Forms.ToolStripStatusLabel empty_status;
        private System.Windows.Forms.TableLayoutPanel buttons_panel;
        private System.Windows.Forms.Button t2_btn;
        private System.Windows.Forms.Button t1_btn;
        private System.Windows.Forms.ToolStripStatusLabel separator;
        private System.Windows.Forms.ToolStripStatusLabel crt_transaction_status;
    }
}

